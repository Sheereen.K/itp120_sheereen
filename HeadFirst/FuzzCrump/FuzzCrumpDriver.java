public class FuzzCrumpDriver
{
    public static void guessAge(FuzzCrump fuzz)
    {
        String info;
        String message = "";
        CliReader reader = new CliReader();

        System.out.println("Time to guess " + fuzz.getMyName() + "'s age.");

        while (message != "You guessed it!")
        {
            info = reader.input("How old do you think I am? ");
            message = fuzz.howOld(Integer.parseInt(info));
            System.out.println(message);
        }

        System.out.println("You took " + fuzz.getGuessCount() + " guesses.\n");
    }

    public static void main(String[] args)
    {
        FuzzCrump fuzz1 = new FuzzCrump("Thing 1");
        FuzzCrump fuzz2 = new FuzzCrump("Thing 2");

        System.out.println(fuzz1.sayHi());
        System.out.println(fuzz2.sayHi());

        guessAge(fuzz1);
        guessAge(fuzz2);
    }
}

