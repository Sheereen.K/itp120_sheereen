public class SimpleDotComGame
{
    public static void main(String[] args)
    {
        String guess;
        String result;
        GameHelper helper = new GameHelper();
        SimpleDotCom game = new SimpleDotCom();

        // choose a random location for the start of the DotCom between 0 and 4
        int start = (int)(Math.random() * 5);

        int[] locations = {start, start + 1, start + 2};
        game.setLocationCells(locations);

        while (!game.killed())
        {
            guess = helper.getUserInput("Enter a number: ");
            System.out.println(game.checkYourself(guess));
        }
        System.out.println("You took " + game.getGuesses() + " guesses");
    }
}

