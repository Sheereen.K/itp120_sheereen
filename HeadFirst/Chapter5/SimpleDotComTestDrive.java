public class SimpleDotComTestDrive
{ 
    public static void main(String[] args)
    {
        SimpleDotCom dot = new SimpleDotCom();
        String userGuess;
        String result;

        int[] locations = {2, 3, 4};
        dot.setLocationCells(locations);

       	userGuess = "4";
        result = dot.checkYourself(userGuess);
        assert result == "hit" : "Expected hit, got " + result;
 	//System.out.println(userGuess);

	userGuess = "2";
        result = dot.checkYourself(userGuess);
        assert result == "hit" : "Expected hit, got " + result;
	//System.out.println(userGuess);

        userGuess = "3";
        result = dot.checkYourself(userGuess);
        assert result == "kill" : "Expected kill, got " + result;
        //System.out.println(userGuess);

       // System.out.println(result);
    }
}

