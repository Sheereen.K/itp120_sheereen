class SimpleDotCom
{
    private int[] locations;
    private int hits = 0;
    private int guesses = 0;

    public void setLocationCells(int[] locations)
    {
        this.locations = locations;
    }

    public int getHits()
    {
        return this.hits;
    }

    public int getGuesses()
    {
        return this.guesses;
    }

    public boolean killed()
    {
        return this.hits > 2;
    }

    public String checkYourself(String userGuess)
    {
        int guess = Integer.parseInt(userGuess);

        // We're processing a guess, so count it.
        this.guesses++;

        for (int location : locations)
        {
            if (location == guess)
            { 
                this.hits++;
                if (this.hits > 2)
                    return "kill";
                return "hit";
            }
        }
        return "miss";
    }
}

