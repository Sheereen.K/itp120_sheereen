class InterleaveStrings
{
    public static String interleave(String s1)
    {
      int stepsTotal = 0;
      int pos = 0;
          // Loop over array from end to start
      for ( int i = a.length-1; i >= 0; i-- )
        {
        if (pos % 2 == 0)
          stepsTotal += a[i] * 3;
        else
          stepsTotal += a[i];
        pos += 1;
        }
      int multipleOf10 = 10 * ( stepsTotal / 10 );
          // If necessary, adjust to the closest multiple of 10 >= stepsTotal
      if ( multipleOf10 != stepsTotal )
           multipleOf10 += 10;

      checkdigit = multipleOf10 - stepsTotal;

    }

    public static void main(String[] args)
    {
        if (args.length == 2)
        {
            System.out.println(interleave(args[0], args[1]));
        }
        else
        {
            System.out.println("Please enter two command line arguments!");
        }
    }
}
