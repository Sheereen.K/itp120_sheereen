public class Card 
{ 
  private String mySuit; 
  private int myValue; 
  private String[] cardNames =  
      { 
        "Deuce", "Three", "Four", "Five", "Six", "Seven", 
        "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace" 
      }; 

  /** 
   * Creates a standard playing card. 
   * 
   * @param suit   A String, either "spades", "hearts", "diamonds", or "clubs" 
   * @param value  An int from 2 through 14 
   */ 
  public Card( String suit, int value ) 
  { 
    mySuit = suit; 
    myValue = value; 
  } 

  /** 
   * Returns a String representation of this card 
   * 
   * @return  A String of the form "<card name> of <suit name>" 
   */ 
  public String name() 
  { 
    return cardNames[ myValue - 2 ] + " of " + mySuit; 
  } 
}
