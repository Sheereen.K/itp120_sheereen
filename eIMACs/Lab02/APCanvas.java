/**
 *
 * @author jelkner
 */
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * A panel on which to draw
 *
 * @author |your name|
 * @version 1.0 |today's date|
 */
public class APCanvas extends JPanel
{
  /**
   * The class constructor
   */
  public APCanvas()
  {
  }

  /**
   * Draws on a Graphics object
   *
   * @param g  a Graphics object
   */
  private void paintMe(Graphics g)
  {
      
      APPoint p = new APPoint( 240, 240 ); 
      double width = 150;
      double height = 100;
      double angle = 0;
      
      for (double i=0 ; i<49; i++)
      { 
      APRectangle r = new APRectangle( p, width, height );
      r.setAngle(angle);
      r.draw( g );
      width *= 0.97;
      height *= 0.97;
      angle += Math.PI / 30;
      }	
  }

  /**
   * Overrides JPanel's paintComponent method
   *
   * @param g  a Graphics object
   */
  public void paintComponent( Graphics g )
  {
    super.paintComponent( g );
    paintMe( g );
  }
}
