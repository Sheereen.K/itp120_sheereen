public class CheckItem
{
  private double myPrice,
                 mySalesTax;

  private int    myQuantity = 1;

  public CheckItem( double price, double salesTax )
  {
    myPrice = price;
    mySalesTax = salesTax;
  }

  public int getQuantity()
  {
    return myQuantity;
  }

  public void setQuantity( int qty )
  {
    myQuantity = qty;
  }

  public double lineItemTotal()
  {
  return roundMoney(myQuantity * myPrice * (100 + mySalesTax) / 100);
  }

  public static double roundMoney( double amount )
  {
    return (int)(100 * amount + 0.5) / 100.0;
  }

  public static void setQuantities( CheckItem[] items, int[] quantities )
  {
    for (int item = 0; item < items.length; item++)
    {
      items[item].setQuantity(quantities[item]);
    }
  }

  public static double[] lineItemTotals( CheckItem[] items )
  {
    double [] a = new double[items.length];
    for (int i=0; i < items.length; i++)
      {
        a[i] = items[i].lineItemTotal();
      }
    return a;
  }
}

