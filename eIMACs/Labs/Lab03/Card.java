public class Card {
  private String mySuit;
  private int myValue;
  private String[] cardNames =
  {
    "Deuce", "Three", "Four", "Five", "Six", "Seven",
    "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"
  };

  public Card( String suit, int value )
    {
      mySuit = suit;
      myValue = value;
    }

  public String name()
    {
      return cardNames[ myValue - 2 ] + " of " + mySuit;
    }
}
