 /**
 *
 * @author Jeff Elkner
 * @version 1.0 2019-10-24
 */
import java.awt.Graphics;

public class APRectangle
{
    private APPoint myTopLeft;
    private double myWidth, myHeight, myAngle;
    
    public APRectangle( APPoint topLeft, double width, double height)
    {
        myTopLeft = topLeft;
        myWidth = width;
        myHeight = height;
    }
    
     public double setAngle(double angle)
    {
        myAngle = angle;
        return myAngle;
    }
    public APPoint getTopLeft()
    {
        return myTopLeft;
    }
    
    public double getWidth()
    {
        return myWidth;
    }
    
    public double getHeight()
    {
        return myHeight;
    }
    
    public void setTopLeft( APPoint topLeft )
    {
        myTopLeft = topLeft;
    }
    
    public void setWidth( double width )
    {
        myWidth = width;
    }
    
    public void setHeight( double height )
    {
         myHeight = height;
    }
    
    public APPoint getTopRight()
    {
        //return new APPoint( myTopLeft.getX() + myWidth, myTopLeft.getY() );
        double x = myWidth * Math.cos( myAngle );
        double y = myWidth * Math.sin( myAngle );

        return new APPoint( myTopLeft.getX() + x, 
                      myTopLeft.getY() - y );
    }
    
    public APPoint getBottomLeft()
    {
        //return new APPoint( myTopLeft.getX(), myTopLeft.getY() + myHeight );
        
        double x = myTopLeft.getX() + myHeight * Math.sin(myAngle);
        double y = myTopLeft.getY() + myHeight * Math.cos(myAngle);
        
        return new APPoint(x, y);
    }
    
    public APPoint getBottomRight()
    {
        //return new APPoint( myTopLeft.getX() + myWidth, myTopLeft.getY() + myHeight );
        APPoint tr = getTopRight();

        double x = myHeight * Math.sin( myAngle );
        double y = myHeight * Math.cos( myAngle );

        return new APPoint( tr.getX() + x, 
                      tr.getY() + y );
    }
    
    public void shrink( double percent )
    {
        myWidth *= percent / 100.0;
        myHeight *= percent / 100.0;
    }
    
    public void draw(Graphics g)
    {
        APPoint topLeft = myTopLeft;
        APPoint topRight = getTopRight();
        APPoint bottomLeft = getBottomLeft();
        APPoint bottomRight = getBottomRight();

        g.drawLine((int)topLeft.getX(), (int)topLeft.getY(),
                   (int)topRight.getX(), (int)topRight.getY());
        g.drawLine((int)topRight.getX(), (int)topRight.getY(),
                   (int)bottomRight.getX(), (int)bottomRight.getY());
        g.drawLine((int)topLeft.getX(), (int)topLeft.getY(),
                   (int)bottomLeft.getX(), (int)bottomLeft.getY());
        g.drawLine((int)bottomLeft.getX(), (int)bottomLeft.getY(),
                   (int)bottomRight.getX(), (int)bottomRight.getY());
    }
}
