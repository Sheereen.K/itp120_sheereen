import java.util.Arrays;

class SwapTester
{
    public static String[] swap1(String d1, String d2)
    {
      String[] a = new String[] {d1, d2};
      a[0] = a[1];
      a[1] = a[0];
      return a;

    }

    public static String[] swap2(String d1, String d2)
    {
      String[] a = new String[] {d1, d2};
      String t = a[0];
      a[0] = a[1];
      a[1] = t;
      return a;
    }

    public static int[] swap3(String d1, String d2)
    {
      int[] a;
      a = new int[2];
      a[0] = Integer.parseInt(d1);
      a[1] = Integer.parseInt(d2);
      a[0] = a[0] - a[1];
      a[1] = a[0] + a[1];
      a[0] = a[1] - a[0];
      return a;
    }

    public static void main(String[] arg)
    {
      if (arg.length == 2) {
        System.out.println(Arrays.toString(swap1(arg[0], arg[1])));
        System.out.println(Arrays.toString(swap2(arg[0], arg[1])));
        System.out.println(Arrays.toString(swap3(arg[0], arg[1])));
      }
      else
        System.out.println("Please give 2 arguments!");
    }
}
